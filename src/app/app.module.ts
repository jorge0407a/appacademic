import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { SettingComponent } from './components/shared/setting/setting.component';
import { ContentComponent } from './components/shared/content/content.component';
import { MenuComponent } from './components/shared/menu/menu.component';
import { InstitutionsComponent } from './components/shared/institutions/institutions.component';
import { PerfilComponent } from './components/shared/perfil/perfil.component';
import { InstitutionService } from './services/institution.service';

const appRoutes: Routes = [
 
  {path: 'content',component: ContentComponent},
  {path: 'institution',component: InstitutionsComponent},
  {path: 'perfil',component: PerfilComponent},
  { path: '',component: ContentComponent,pathMatch: 'full'},
  { path: '**',
  redirectTo: '/',
  pathMatch: 'full'
},
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SettingComponent,
    ContentComponent,
    MenuComponent,
    InstitutionsComponent,
    PerfilComponent,
 
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
    HttpClientModule
  ],
  providers: [
    InstitutionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
