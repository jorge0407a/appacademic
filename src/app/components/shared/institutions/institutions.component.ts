import { Component, OnInit } from '@angular/core';
import { Institution} from '../../../models/institution';
import { InstitutionService} from '../../../services/institution.service';

@Component({
  selector: 'app-institutions',
  templateUrl: './institutions.component.html'
})
export class InstitutionsComponent implements OnInit {
  
  intitutions = null;
  institution ={
    idInstitution : null,
    name : null,
    ruc : null
  }
  /**institutionArray : Institution[]=[
    {idInstitution:1,name:"Diego Thomson",ruc:"2332323"},
    {idInstitution:1,name:"Diego Thomson 2",ruc:"2332323"},
    {idInstitution:1,name:"Diego Thomson 3",ruc:"2332323"},
  ]**/
  constructor(private institutionService : InstitutionService) { }

  ngOnInit(): void {
    this.getInstitution();
  }
  getInstitution(){
    this.institutionService.getInstitutions().subscribe(
      result => this.intitutions = result
    );
  }
  nullReg() {
    if(this.institution == null) {
      return false;
    } else {
      return true;
    }
  }

}
