import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'  
})
export class InstitutionService {
URL = "http://localhost/API/";
  constructor(private http: HttpClient) {
   }
   
   getInstitutions(){
    return this.http.get(`${this.URL}getInstitution.php`);
  }

}
